#!/bin/sh

errors=0

fatal() {
  echo "$@" 1>&2
  exit 1
}

compare() {
  local infile="$1"
  local reffile="$2"
  [ -r "${infile}" ] || fatal "cannot read MIDIfile '${infile}'"
  [ -r "${reffile}" ] || fatal "cannot read reference file '${reffile}'"
  local outfile=$(mktemp)
  echo "comparing '${infile}' with '${reffile}' via '${outfile}'"
  mftext "${infile}" > "${outfile}"
  diff -bBw "${outfile}" "${reffile}" 1>&2 || errors=1
  rm "${outfile}" "${infile}"
}


doit() {
  echo "========= $1 ========="
  python3 "$1" || exit 1
  compare "$2" "$3"
  echo ""
}

doit examples/single-note-example.py output.mid debian/tests/output.txt
doit examples/c-major-scale.py major-scale.mid debian/tests/major-scale.txt

exit $errors
