Header format=1 ntrks=2 division=960
Track start
Time=0 Tempo, microseconds-per-MIDI-quarter-note=1000000
Time=0 Meta event, end of track
Track end
Track start
Time=0 Note on, chan=1 pitch=60 vol=100
Time=960 Note off, chan=1 pitch=60 vol=100
Time=960 Note on, chan=1 pitch=62 vol=100
Time=1920 Note off, chan=1 pitch=62 vol=100
Time=1920 Note on, chan=1 pitch=64 vol=100
Time=2880 Note off, chan=1 pitch=64 vol=100
Time=2880 Note on, chan=1 pitch=65 vol=100
Time=3840 Note off, chan=1 pitch=65 vol=100
Time=3840 Note on, chan=1 pitch=67 vol=100
Time=4800 Note off, chan=1 pitch=67 vol=100
Time=4800 Note on, chan=1 pitch=69 vol=100
Time=5760 Note off, chan=1 pitch=69 vol=100
Time=5760 Note on, chan=1 pitch=71 vol=100
Time=6720 Note off, chan=1 pitch=71 vol=100
Time=6720 Note on, chan=1 pitch=72 vol=100
Time=7680 Note off, chan=1 pitch=72 vol=100
Time=7680 Meta event, end of track
Track end
